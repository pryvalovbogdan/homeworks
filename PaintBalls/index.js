var canvas = document.getElementById("canvas1");
var arr = [];
var ctx = canvas.getContext("2d");

var CreateBall = function(){
    this.x =getRandomX();
    this.y =getRandomY();
    this.radius = getRandomRadius();
    this.dx = 2;
    this.dy = -2;
    this.color = getRandomColor();
};

CreateBall.prototype.draw = function () {

    if(this.x + this.dx > canvas.width-this.radius || this.x + this.dx < this.radius) {
        this.dx = -this.dx;
    }
    if(this.y + this.dy > canvas.height-this.radius || this.y + this.dy < this.radius) {
        this.dy = -this.dy;
    }
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.fillStyle = this.color;

    this.x += this.dx;
    this.y += this.dy;

};

drawBall = (e) =>{
    var ball = new CreateBall();
    arr.push(ball);
};

setInterval(()=>{drawAllBalls(arr)}, 10);

drawAllBalls = (arr) =>{

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for(let i = 0; i < arr.length; i++) {
        let balls = arr[i];
        balls.draw()
    }
};

getRandomX = () =>{
    return Math.floor(Math.random() * 420) + 40
};

getRandomY = () =>{
    return  Math.floor(Math.random() * 200) + 40
};

getRandomRadius = () =>{
    return Math.floor(Math.random() * 25)
};

getRandomColor = () =>{
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

canvas.addEventListener("click", drawBall);