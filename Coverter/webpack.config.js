const HtmlWebpackPlugin = require('html-webpack-plugin'); // Require  html-webpack-plugin plugin
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require("path");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    entry: __dirname + "/src/index.js", // webpack entry point. Module to start building dependency graph
         output: {
             path: __dirname + '/dist', // Folder to store generated bundle
             filename: 'bundle.js',  // Name of generated bundle after build
             publicPath: '/' // public URL of the output directory when referenced in a browser
         },
         module: {  // where we defined file patterns and their loaders
             rules: [
                 {
                     test: /\.js$/,
                     exclude: /(node_modules|bower_components)/,
                     use: {
                         loader: 'babel-loader',
                         options: {
                             presets: ['@babel/preset-env']
                         }
                     }
                 },
                 {
                 test: /\.less$/,
                 use: [
                     MiniCssExtractPlugin.loader,
                     {
                     loader: 'css-loader', // translates CSS into CommonJS

                 }, {
                     loader: 'less-loader', // compiles Less to CSS

                 }]

             }]
         },
    plugins: [  // Array of plugins to apply to build chunk
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: __dirname + "/public/index.html",
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
    ],
   devServer: {  // configuration for webpack-dev-server
            contentBase: './public',  //source of static assets
            port: 7700, // port to run dev-server
         }
}
