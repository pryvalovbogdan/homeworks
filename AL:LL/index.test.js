var app = require("./index")

test("test array init",async ()=>{
    app.testArr.init(5,2,3);
    expect(app.testArr.array).toEqual([5,2,3]);
});


test("Shift LinkedList sould return [2] if you console it",async ()=>{
    app.testLinkedList.unshift([1]);
    app.testLinkedList.unshift([2]);

    expect(app.testLinkedList.shift()).toEqual([2]);

});
test("After LinkedList shift root must to be [1] ",async ()=>{
    app.testLinkedList.unshift([1]);
    app.testLinkedList.unshift([2]);
    app.testLinkedList.shift();
    expect(app.testLinkedList.root.value).toEqual([1])
});

test("After LinkedList shift root must to be [5] ",async ()=>{

    expect(app.testArr.slice(0,1)).toEqual([5])
});

test("After LinkedList shift root must to be [2,3] ",async ()=>{

    expect(app.testArr.slice(1)).toEqual([2,3])
});
test("Should return [2,3] after slice(-2) ",async ()=>{

    expect(app.testArr.slice(-2)).toEqual([2,3])
});
describe("test array getsize func", ()=>{
    test("Should return 3 length for [5,2,3] ",async ()=>{

        expect(app.testArr.getSize()).toBe(3);
    });
});
describe("test array pop func", ()=>{
    test("Should return value 3 of deleted elem from  [5,2,3] ",async ()=>{

        expect(app.testArr.pop()).toBe(3);
    });
    test("Should return 2 length for array after pop ",async ()=>{

        expect(app.testArr.getSize()).toBe(2);
    });
});

describe("test array unshift func", ()=>{
    test("Should return 4 length after unshift 2,3",async ()=>{

        expect(app.testArr.unshift(2,3)).toBe(4);
    });
    test("Should return [2,3,5,2]  after unshift 2,3",async ()=>{

        expect(app.testArr.array).toEqual([2,3,5,2]);
    });
});


describe("test array unshift func", ()=>{
    test("Should return 4 length after unshift 2,3",async ()=>{

        expect(app.testArr.unshift(2,3)).toBe(6);
    });
    test("Should return [2,3,5,2]  after unshift 2,3",async ()=>{

        expect(app.testArr.array).toEqual([2,3,2,3,5,2]);
    });
});

describe("test array shift func", ()=>{
    test("Should return 2: deleted value, after shift [2,3,5,2]",async ()=>{

        expect(app.testArr.shift()).toBe(2);
    });
    test("Should return [3,2,3,5,2]  after shift",async ()=>{

        expect(app.testArr.array).toEqual([3,2,3,5,2]);
    });
});

describe("test array get func", ()=>{
    test("Should return 3 if you get by 2 index",async ()=>{

        expect(app.testArr.get(2)).toBe(3);
    });
    test("Should return 2 if you get by -1 index",async ()=>{

        expect(app.testArr.get(-1)).toEqual(2);
    });
    test("Should return 3 if you get by 0 index",async ()=>{

        expect(app.testArr.get(0)).toEqual(3);
    });
});

describe("test array set func", ()=>{
    test("Should return length 6 fater set element on 2 index",async ()=>{

        expect(app.testArr.set(2, 34)).toBe(6);
    });
    test("Should return [3,2,34,3,5,2] after setted el on 2 index",async ()=>{

        expect(app.testArr.array).toEqual([3,2,34,3,5,2]);
    });
});

describe("test array sort func", ()=>{

    test("Should return [2,2,3,3,5,34] after serted [3,2,34,3,5,2]",async ()=>{

        expect(app.testArr.sort()).toEqual([2,2,3,3,5,34]);
    });
});