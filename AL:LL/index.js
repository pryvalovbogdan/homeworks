
function Ilist() {

}

Ilist.prototype.init = function () {};

Ilist.prototype.getSize = function () {};

Ilist.prototype.push = function () {};

Ilist.prototype.pop = function () {};

Ilist.prototype.shift = function () {};

Ilist.prototype.unshift = function () {};

Ilist.prototype.slice = function () {};

Ilist.prototype.splice = function () {};

Ilist.prototype.sort = function () {};

Ilist.prototype.get = function () {};

Ilist.prototype.set = function () {};




function ArrayList() {
    Ilist.apply(this, arguments);
    this.array = [];
}

ArrayList.prototype = Object.create(Ilist.prototype);
ArrayList.prototype.constructor = ArrayList;
ArrayList.prototype.get = function(index){
    var res;
    if(index >= 0){
        res = this.array[index];
    } else {
        res = this.array[this.array.length - Math.abs(index)]
    }
    return res
};
ArrayList.prototype.set = function(index, value){
        var temp = [];
        temp[0] = this.array[index];
        if(index >= this.array.length) {
            this.array[index] = value;
        } else {
            this.array[index] = value;
            this.array.length = this.array.length + 1;
            for(var i = index + 1; i < this.array.length; i++ ){
                temp[i - index] = this.array[i];
                this.array[i] = temp[i - index - 1];
            }
        }
    return this.array.length;
};
ArrayList.prototype.sort = function(){
    if(arguments.length ===0){
        for (var i = 0; i < this.array.length; i++) {
            for (var j = i + 1; j < this.array.length; j++) {
                if (this.array[i] > this.array[j]) {
                    var temp = this.array[j];
                    var temp2 = this.array[i];
                    this.array[i] = temp;
                    this.array[j] = temp2;
                }

            }
        }
        return this.array;
    }   else {
        var res = [];
        var func = arguments[0];
        for(var i = 0; i < this.array.length; i++) {
           res[i] = func(this.array[i], this.array[i + 1])
        }
        return res
    }
};
ArrayList.prototype.push = function(value){
    this.array[this.array.length] = value;
};
ArrayList.prototype.pop = function(){
    var temp = this.array[this.array.length - 1];
    this.array.length = this.array.length - 1;
    return  temp;
};
ArrayList.prototype.unshift = function(){

    for(var j = arguments.length -1; j >= 0; j--) {
        for(var i = this.array.length - 1; i > -1 ; i--){
            this.array[i + 1] = this.array[i]
        }
        this.array[0] = arguments[j]
    }
    return this.array.length;
};

ArrayList.prototype.shift = function(){
    var deleted = this.array[0];
    var temp = [];
    for (var i = 1; i < this.array.length; i++){
        temp[i - 1]  = this.array[i];
       this.array[i - 1] = temp[i - 1];
    }
    this.array.length = this.array.length -1;
    return deleted;
};

ArrayList.prototype.init = function () {
    for(var i = 0; i < arguments.length; i++){
        this.array.push(arguments[i]);
    }
};
ArrayList.prototype.getSize = function(){
    return this.array.length;
};

ArrayList.prototype.slice = function () {
    var i;
    var res = [];
    if(arguments[0] >= 0){
        i = arguments[0];
    } else if (arguments[0] < 0){
        i = this.array.length - Math.abs(arguments[0])
    }
    for ( ; i < this.array.length; i++) {
        if (arguments[1] === undefined) {
            res.push(this.array[i]);
        } else if (arguments[1] > 0) {
            while (arguments[1] > 0) {
                res.push(this.array[i]);
                arguments[1]--;
                i++
            }
        } else if (arguments[0] < 0) {
                res.push(this.array[i]);
        }
    }
    return res;

};

ArrayList.prototype.splice = function (firstInd, secondInd, value) {
    var temp = [];
    if(!value){
        for (var i = 0; i < firstInd; i++){
                temp[i] = this.array[i]
        }
        for (var i = firstInd + secondInd; i < this.array.length; i++){
            temp[i - secondInd] = this.array[i]
        }
        this.array.length = this.array.length - secondInd;
        for (var i = 0; i < this.array.length; i++){
            this.array[i] = temp[i];
        }
    } else {
        for (var i = 0; i < firstInd; i++){
            temp[i] = this.array[i];
            //console.log(temp)
        }
        temp[firstInd] = value;
        //console.log(temp)
        for (var i = firstInd  ; i < this.array.length; i++){
            temp[i + secondInd] = this.array[i + 1]
        }
        //this.array.length = this.array.length + 1 - (secondInd - firstInd);
        for (var i = 0; i < this.array.length; i++){
            this.array[i] = temp[i];
        }
    }
    return this.array
};

var res = new ArrayList();
res.init(1,7,3,8);
console.log(res.array);
console.log(res.slice(-3));
console.log(res.unshift(9,3, 3))
console.log(res.shift());
console.log(res.array)
console.log(res.set(6,11));
console.log(res.sort());
console.log(res.splice(3,1, 99))
console.log(res.splice(3,1))
//console.log(res.sort((a,b)=>{a - b}))
// LinkedList
var Node = function(value) {
    this.value = value;
    this.next = null;
};
var LinkedList = function() {
    Ilist.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(Ilist.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    var tempNode = this.root;
    var size = 0;

    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};
LinkedList.prototype.push = function(value) {
    var allNodes = this.root;
    var size = 0;
     var tempNode = this.root.next;

    while(tempNode === null) {
        tempNode = allNodes;
         size++;
    }

    var node = new Node(value);
    tempNode.next = node;
    return size;
};


LinkedList.prototype.pop = function() {
    var allNodes = this.root;
    var size = 0;
    var tempNode = this.root.next;

    while(tempNode === null) {
        tempNode = allNodes;
        size++;

    }
    tempNode.next = null;

    return size -1 ;
};

LinkedList.prototype.unshift = function(value) {
    var size = this.getSize();
    var node = new Node(value);
    node.next = this.root;
    this.root = node;
    return size ;
};

LinkedList.prototype.init = function(initialArray) {
    for (var i = initialArray.length - 1 ; i >= 0; i--) {
        this.unshift(initialArray[i]);
    }
};

LinkedList.prototype.shift = function() {
    var allNodes = this.root;
    var tempNode = this.root.next;
    this.root = tempNode;
    tempNode = tempNode.next;
    return allNodes.value;
};

LinkedList.prototype.get = function (index) {
    var counter = 0;
    var node = this.root;
        while (node !== null) {
        if (counter === index) {
            return node;
        }
        counter++;
        node = node.next;
    }
    return null;
};


LinkedList.prototype.set = function (index, value) {
    var counter = 0;
    var node = this.root;
    var newNode = new Node(value);
    var temp;
    var temp2;
    while (node!== null) {
        if (counter === index - 1) {
            temp = node;
            newNode.next = temp;
            this.root = newNode;
        }
        counter++;
        node = node.next;
    }
    return this.root;

};

LinkedList.prototype.slice = function (indexStart, indexEnd) {
    var counter = 0;
    var node = this.root;
    var temp;
    var temp2;
    if(indexEnd === null){
        while (node!== null) {
            if (counter === indexStart) {
                temp = node;
                this.root = temp;
            }
            counter++;
            node = node.next;
        }
    } else if(indexEnd !== null && indexStart !== null) {
        while (node !== null) {
            if (counter === indexStart) {
                temp = node;
                this.root = temp;
            }
            if (counter === indexEnd - 1) {
                temp2 = node;
                temp2.next = null

            }
            counter++;
            node = node.next;
        }
    }

    return this.root;
};

LinkedList.prototype.splice = function (indexStart, indexEnd, value) {
    var counter = 0;
    var node = this.root;
    var newNode = new Node(value);
    var temp;
    var temp2;
    if(indexEnd === 0){
        while (node!== null) {
            if (counter === indexStart) {
                temp = node;
                newNode.next = temp;
                this.root = newNode;
            }
            counter++;
            node = node.next;
        }
    } else  {
        while (node !== null) {
            if (counter === indexStart) {
                temp = node;
                newNode.next = temp;

            }
            if (counter === indexEnd) {
                temp2 = node;
                newNode.next = temp2;
                this.root = newNode;
            }
            counter++;
            node = node.next;
        }
    }

    return this.root;
};


var testArr = new ArrayList();
var testLinkedList = new LinkedList();
testLinkedList.init([2,5,11,6, 1]);
console.log(testLinkedList.slice(1));
console.log(testLinkedList.splice(1,1,33));
console.log(testLinkedList.set(1,99));
console.log(testLinkedList.set(1,99));



module.exports = {
    testArr, testLinkedList
};