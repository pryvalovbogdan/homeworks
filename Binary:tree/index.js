function BSTree() {
      this.root = null;
}

var Node = function(value) {
  this.value = value;
  this.right = null;
  this.left= null;
};

BSTree.prototype.init = function (array) {
    this.clear();
    for (var i = 0; i < array.length; i++) {
        this.add(array[i]);
    }
};

BSTree.prototype.clear = function (array) {
    this.root = null;
};

BSTree.prototype.add = function(value) {
    if (value === null) {
        return;
    }
    this.root = this.addNode(this.root, value);
};

BSTree.prototype.addNode = function(node, value) {
    if (node === null) {
        node = new Node(value);
    }
    else if (value < node.value) {
        node.left = this.addNode(node.left, value);
    } else {
        node.right = this.addNode(node.right, value);
    }
    return node;
};

BSTree.prototype.getHeight = function () {
    return this.height(this.root);
};

BSTree.prototype.height = function (node) {
    if (node === null) {
        return 0;
    }
    var leftCount = 0;
    var rightCount = 0;

    if (node.left !== null) {
        leftCount = this.height(node.left);
    }
    if (node.right !== null) {
        rightCount = this.height(node.right);
    }
    return leftCount > rightCount ? leftCount + 1 : rightCount + 1;
};


    BSTree.prototype.find = function (value) {
    if (this.root.left !== null || this.root.right !== null) {
        if (this.root.value >= value) {
            //console.log(this.root);
            if (this.root.value === value) {
                return this.root;
            }
            else {
                this.root = this.root.left;
                this.find(value);
            }
        } else if (this.root.value < value) {
            if (this.root.value === value) {
                return this.root;
            }
            else {
                this.root = this.root.right;
                this.find(value);
            }
        }
    }
    return this.root
};

BSTree.prototype.getLeaves = function() {
    return this.leaves(this.root, 1);
};

BSTree.prototype.leaves = function(node){
    if(node === null){
        return 0;
    }
    if(node.left === null && node.right === null){
        return 1;
    } else {
        return this.leaves(node.left) + this.leaves(node.right)
    }
};

BSTree.prototype.reverseSup = function () {
   return this.reverse(this.root);
};

BSTree.prototype.reverse = function (node) {
   if(node === null) {
       return;
   }
    var tempNode = new BSTree();
    tempNode = node.left;
    node.left = node.right;
    node.right = tempNode;

    if(node.left !== null) {
        this.reverse(node.left);
    }
    if(node.right !== null) {
        this.reverse(node.right)
    }
};

BSTree.prototype.getNodes = function () {
    return this.nodes(this.root);
};

BSTree.prototype.nodes = function (node) {
    if (node === null) {
        return null;
    }
    if (node.left === null && node.right === null) {
        return 1;
    }
    if(node.left === null) {
        return 1 + this.nodes(node.right);
    }
    if(node.right === null) {
        return 1 + this.nodes(node.left);
    }
        return 1 + this.nodes(node.left) + this.nodes(node.right);
};

BSTree.prototype.toArray = function () {
    var theArray = [];
      return this.array(this.root, theArray);
};

BSTree.prototype.array = function (node, arr) {
    if (node !== null) {
        arr.push(node.value);
        if(node.left !== null) {
          this.array(node.left, arr);
        }
        if(node.right !== null) {
            this.array(node.right, arr);
        }
    }
    return arr;
};

BSTree.prototype.remove = function (value) {
    return this.removeSup(this.root, value);
};

BSTree.prototype.removeSup = function (node, value) {
    if (node === null)
        return node;

    if (value <  node.value) {
        node.left = this.removeSup(node.left, value);
    } else if (value > node.value) {
        node.right = this.removeSup(node.right, value);
    } else {
        if (node.left === null) {
            return node.right;
        } else if (node.right === null)
            return node.left;

        node.value = this.inOrderSuccessor(node.right);
        node.right = this.removeSup(node.right, node.value);
    }
    return node;
};

BSTree.prototype.inOrderSuccessor = function (node) {
    var minimum = node.value;
    while (node.left !== null) {
        minimum = node.left.value;
        node = node.left;
    }
    return minimum;
};

BSTree.prototype.print = function (value) {
    var findNode = this.find(value);
    var resultString = "";

    resultString += findNode.value;

    return resultString;
};

BSTree.prototype.getWidth = function () {
    var height = this.getHeight();
    var widthMax = 0;
    var width;
    for (var i = 1; i <= height; i++) {
        width = this.width(this.root, i);
        if (width > widthMax) {
            widthMax = width;
        }
    }
    return widthMax;
};

BSTree.prototype.width = function (node, level) {
    if(node === null) {
        return 0;
    }
    if (level === 1) {
        return 1;
    }
    return this.width(node.left, level - 1) + this.width(node.right, level - 1)
};

var tree = new BSTree();
tree.init([25, 37,19,45,31,18,14,15]);

export {BSTree, Node};