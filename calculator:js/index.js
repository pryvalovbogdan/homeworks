const output =  document.getElementById('output');
const inputs = document.querySelectorAll(".buttons");
const calc = document.getElementById("calc");
const clear = document.getElementById("clear");
[].forEach.call(inputs,function(el){
    el.addEventListener('click',  e => {

        if(output.value.length >=20 ){
            alert("too big number")
        } else {
            output.value += e.target.value;
        }
     })
});

calc.addEventListener("click", () => {
    if(output.value.length === 0) return false;
    output.value = eval(output.value);
});

clear.addEventListener("click", () =>{
    output.value = "";
});

import "./public/style/index.less";