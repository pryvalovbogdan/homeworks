var app = require("../oldcalc");
var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var dom = new JSDOM(`<!DOCTYPE html><p>Hello world</p>`);

describe("testing  sum func", ()=> {
    test("sum of 4 and 2 should return 6",async ()=>{
        expect(app.sum(4,2)).toBe(6);
    });
    test("sum of -4 and -2 should return -6",async ()=>{
        expect(app.sum(-4,-2)).toBe(-6);
    });
});
describe("testing  multiply func", ()=> {
    test("multiply of 3 and 2 should return 6",async ()=>{
        expect(app.multiply(3,2)).toBe(6);
    });
    test("multiply of 3 and 2 should return 6",async ()=>{
        expect(app.multiply(-3,-2)).toBe(6);
    });
});
describe("testing  divine func", ()=> {
    test("divine of 4 and 2 should return 2",async ()=>{
        expect(app.divine(4,2)).toBe(2);
    });
});
describe("testing  minus func", ()=> {
    test("minus of 4 and 1 should return 3",async ()=>{
        expect(app.minus(4,1)).toBe(3);
    });
});