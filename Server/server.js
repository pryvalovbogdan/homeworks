

var express = require("express");
var app = express();
var path = require("path")
var bodyParser = require("body-parser");

app.listen(3000, function () {
    console.log("port: " + 3000)
});

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'dist')));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.post("/plus", function (req, res) {
    console.log(req.body);
    var result;
    switch (req.body.operator) {
        case "/":
        result = req.body.first / req.body.second;
        break;
        case "-":
        result = req.body.first - req.body.second;
        break;
        case "+":
            result = req.body.first + req.body.second;
            break;
        case "*":
            result = req.body.first * req.body.second;
            break;
   }

    res.json(result)
});
