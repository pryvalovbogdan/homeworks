var assert = require("chai").assert;
var app = require("../script");

describe("checkValueNames", function () {
    it("if parameters not Number expected undefined", function() {
        assert.typeOf(app.checkIsValueInArrays(), undefined)
    });
    it("should return четыре if parameters: 4", function () {
        assert.equal(app.checkIsValueInArrays("rrrd"), 2)
    });
    it("should return двадцать семь  if parameters: 27 ", function () {
        assert.equal(app.checkIsValueInArrays("foot"), 4)
    });
    it("should return двадцать семь  if parameters: 27 ", function () {
        assert.equal(app.checkIsValueInArrays(), 0)
    });
});


