

var buttonGenerate = document.getElementById("generate");
var buttonReset = document.getElementById("reset");

var minIput = document.getElementById("min");
var maxIput = document.getElementById("max");
var result = document.getElementById("result");

var arr = [];
function random() {
    var min = minIput.value;
    var max = maxIput.value;
    var res;
    if (arr.length === (max - min) + 1) {
        return result.innerHTML ="Maximum number of generations!!!"
    }
    res = Math.round( min - 0.5 + Math.random() * (max - min + 1));
    if (checkIsValueInArray(arr, res)) {
        return result.innerHTML ="Generate again"
    } else {
        arr.push(res);
        result.innerHTML = res;
    }
}
function checkIsValueInArray(arr, res) {
     for(var i = 0; i < arr.length; i++) {
         if(arr[i] === res) {
             return true;
         }
     }
     return false;
 }
function reset(){
    minIput.value = "";
    maxIput.value = "";
    result.innerHTML = ""
}
buttonGenerate.addEventListener("click", random);
buttonReset.addEventListener("click", reset);



module.exports = {
    checkIsValueInArrays: checkIsValueInArray
}