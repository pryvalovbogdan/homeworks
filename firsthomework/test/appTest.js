const assert = require("chai").assert;
const app = require("../app");


describe("testing app module", function () {
    describe("getResultEvenNumber", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getResultEvenNumber(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getResultEvenNumber(""), undefined)
        });
        it("should return 6 if parameters equal to 2 and 3", function () {
            assert.equal(app.getResultEvenNumber(2, 3), 6)
        });
        it("should return 6 if parameters equal to 2 and 3", function () {
            assert.equal(app.getResultEvenNumber(7, 3), 10)
        });
    });

    describe("getPartByCoordinates", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getPartByCoordinates(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getPartByCoordinates(""), undefined)
        });
        it("should return x:0, first && second quarter if parameters : x = 0, y > 0", function () {
            assert.equal(app.getPartByCoordinates(0, 1), "x:0, first && second quarter")
        });
        it("should return x:0, third && forth quarter if parameters : x = 0, y < 0", function () {
            assert.equal(app.getPartByCoordinates(0, -1), "x:0, third && forth quarter")
        });
        it("should return y:0, first && second quarter if parameters : x > 0, y = 0", function () {
            assert.equal(app.getPartByCoordinates(1, 0), "y:0, first && second quarter")
        });
        it("should return y:0, third && forth quarter if parameters : x < 0, y = 0", function () {
            assert.equal(app.getPartByCoordinates(-1, 0), "y:0, third && forth quarter")
        });
        it("should return x:0, y:0 if parameters : x = 0, y = 0", function () {
            assert.equal(app.getPartByCoordinates(0, 0), "x:0, y:0")
        });
        it("should return first quarter if parameters : x = 7, y = 3", function () {
            var result = app.getPartByCoordinates(7, 3);
            assert.equal(result, "first quarter")
        });
        it("should return second quarter if parameters : x = -1, y = 3", function () {
            var result = app.getPartByCoordinates(-1, 3);
            assert.equal(result, "second quarter")
        });
        it("should return third quarter if parameters : x = -7, y = -3", function () {
            var result = app.getPartByCoordinates(-7, -3);
            assert.equal(result, "third quarter")
        });
        it("should return forth quarter if parameters : x = 7, y = -3", function () {
            var result = app.getPartByCoordinates(7, -3);
            assert.equal(result, "forth quarter")
        });
    });

    describe("getSumOfPositiveNumbers", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getSumOfPositiveNumbers(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getSumOfPositiveNumbers(""), undefined)
        });
        it("should return 5 if parameters empty string -1, 2, 3", function () {
            var result = app.getSumOfPositiveNumbers(-1, 2,3);
            assert.equal(result, 5)
        });
        it("should return 3 if parameters empty string -1, -2, 3", function () {
            var result = app.getSumOfPositiveNumbers(-1, -2,3);
            assert.equal(result, 3)
        });
        it("should return 0 if parameters empty string -1, -2, -3", function () {
            var result = app.getSumOfPositiveNumbers(-1, -2,-3);
            assert.equal(result, 0)
        });
    });
    describe("maxOfThree", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.maxOfThree(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.maxOfThree(""), undefined)
        });
        it("should return 27 if parameters empty string 2, 3, 4", function () {
            assert.equal(app.maxOfThree(2, 3, 4), 27)
        });
        it("should return 6 if parameters empty string 1, 1, 1", function () {
            assert.equal(app.maxOfThree(1, 1, 1), 6)
        });
        it("should return same value if parameters empty string 1, 2, 3", function () {
            assert.equal(app.maxOfThree(1, 2,3), "same value")
        });
        it("should return same value if parameters empty string 1, 2, 3", function () {
            assert.equal(app.maxOfThree(-1, -2,-3), "same value")
        });
    });
    describe("studyRate", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.studyRate(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.studyRate(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -10", function () {
            assert.equal(app.studyRate(-10), undefined)
        });
        it("should return rate:F if parameters empty string 10", function () {
            assert.equal(app.studyRate(10), "F")
        });
        it("should return rate:E if parameters empty string 21", function () {
            assert.equal(app.studyRate(21), "E")
        });
        it("should return rate:D if parameters empty string 44", function () {
            assert.equal(app.studyRate(44), "D")
        });
        it("should return rate:B if parameters empty string 73", function () {
            assert.equal(app.studyRate(73), "B")
        });
        it("should return rate:A if parameters empty string 98", function () {
            assert.equal(app.studyRate(98), "A")
        });
    });
    describe("sumEvenNumbers", function () {
        it("сумма четных в диапазоне: 2450", function () {
            var result = app.sumEvenNumbers();
            assert.equal(result, 2450)
        });
    });
    describe("isSimpleNum", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.isSimpleNum(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.isSimpleNum(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -2", function () {
            assert.equal(app.isSimpleNum(-2), undefined)
        });
        it("should return Prime number if parameters empty string 2", function () {
            assert.equal(app.isSimpleNum(2), "Prime number")
        });
        it("should return Not prime number if parameters empty string 4", function () {
            assert.equal(app.isSimpleNum(4), "Not prime number")
        });
    });
    describe("mathSqrt", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getMathSqrt(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getMathSqrt(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -2", function () {
            assert.equal(app.getMathSqrt(-2), undefined)
        });
        it("should return 2 if parameters empty string 4", function () {;
            assert.equal(app.getMathSqrt(4), 2);
        });
        it("should return 3 if parameters empty string 9", function () {
            assert.equal(app.getMathSqrt(9), 3)
        });
    });
    describe("getFactorial", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getFactorial(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getFactorial(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -2", function () {
            assert.equal(app.getFactorial(-2), undefined)
        });
        it("should return 24 if parameters empty string 4", function () {
            var result = app.getFactorial(4);
            assert.equal(result, 24)
        });

    });
    describe("getSumOfNumbers", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.getSumOfNumbers(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getSumOfNumbers(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -2", function () {
            assert.equal(app.getSumOfNumbers(-2), undefined)
        });
        it("should return 7 if parameters empty string 124", function () {
            assert.equal(app.getSumOfNumbers(124), 7)
        });
    });
    describe("reverseNumber", function () {
        it("if parameters not a number expected undefined", function() {
            assert.equal(app.reverseNumber(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.reverseNumber(""), undefined)
        });
        it("should return undefined if parameters empty string negative: -2", function () {
            assert.equal(app.reverseNumber(-2), undefined)
        });
        it("should return 421 if parameters empty string 124", function () {
            assert.equal(app.reverseNumber(124), 421)
        });
    });
    describe("getMaxElementOfArray", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.getMaxElementOfArray([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.getMaxElementOfArray("some string"), undefined)
        });
        it("should return 8 if parameters empty string [1,8,3,4]", function () {
            assert.equal(app.getMaxElementOfArray([1,8,3,4]), 8)
        });
        it("should return -1 if parameters empty string [-1,-2,-3,-4]", function () {
            assert.equal(app.getMaxElementOfArray([-1,-2,-3,-4]), -1)
        });
    });
    describe("getMinElementOfArray", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.getMinElementOfArray([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.getMinElementOfArray("some string"), undefined)
        });
        it("should return 1 if parameters empty string [1,8,3,4]", function () {
            assert.equal(app.getMinElementOfArray([1,8,3,4]), 1)
        });
        it("should return -8 if parameters empty string [-1,-8,-3,-4]", function () {
            assert.equal(app.getMinElementOfArray([-1,-8,-3,-4]), -8)
        });
    });
    describe("getMinNumberIndex", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.getMinNumberIndex([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.getMinNumberIndex("some string"), undefined)
        });
        it("should return 0 if parameters empty string [1,8,3,4]", function () {
            assert.equal(app.getMinNumberIndex([1,8,3,4]), 0)
        });
        it("should return 1 if parameters empty string [-1,-8,-3,-4]", function () {
            assert.equal(app.getMinNumberIndex([-1,-8,-3,-4]), 1)
        });
    });
    describe("getMaxNumberIndex", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.getMaxNumberIndex([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.getMaxNumberIndex("some string"), undefined)
        });
        it("should return 1 if parameters empty string [1,8,3,4]", function () {
            assert.equal(app.getMaxNumberIndex([1,8,3,4]), 1)
        });
        it("should return 0 if parameters [7,2,3,4]", function () {
            assert.equal(app.getMaxNumberIndex([7,2,3,4]), 0)
        });
    });
    describe("bubbleSort", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.bubbleSort([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.bubbleSort("some string"), undefined)
        });
        it("should return [1,3,4,8] if parameters empty string [1,8,3,4]", function () {
            assert.deepEqual(app.bubbleSort([1,8,3,4]), [1,3,4,8])
        });
        it("should return [-7,-4,-3,2] if parameters [7,2,3,4]", function () {
            assert.deepEqual(app.bubbleSort([-7,2,-3,-4]), [-7,-4,-3,2])
        });
    });
    describe("insertSort", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.insertSort([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.insertSort("some string"), undefined)
        });
        it("should return [1,3,4,8] if parameters empty string [1,8,3,4]", function () {
            assert.deepEqual(app.insertSort([1,8,3,4]), [1,3,4,8])
        });
        it("should return [-7,-4,-3,2] if parameters [7,2,3,4]", function () {
            assert.deepEqual(app.insertSort([-7,2,-3,-4]), [-7,-4,-3,2])
        });
    });
    describe("selectSort", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.selectSort([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.selectSort("some string"), undefined)
        });
        it("should return [1,3,4,8] if parameters empty string [1,8,3,4]", function () {
            assert.deepEqual(app.selectSort([1,8,3,4]), [1,3,4,8])
        });
        it("should return [-7,-4,-3,2] if parameters [7,2,3,4]", function () {
            assert.deepEqual(app.selectSort([-7,2,-3,-4]), [-7,-4,-3,2])
        });
    });
    describe("sumOfOddIndexElements", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.sumOfOddIndexElements([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.sumOfOddIndexElements("some string"), undefined)
        });
        it("should return 12 if parameters empty string [1,8,3,4]", function () {
            assert.equal(app.sumOfOddIndexElements([1,8,3,4]), 12)
        });
        it("should return 6 if parameters empty string [7,2,3,4]", function () {
            assert.equal(app.sumOfOddIndexElements([7,2,3,4]), 6)
        });
    });
    describe("getNotEvenElementsNumber", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.getNotEvenElementsNumber([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.getNotEvenElementsNumber("some string"), undefined)
        });
        it("should return 2 if parameters [1,8,3,4] ", function () {
            assert.equal(app.getNotEvenElementsNumber([1,8,3,4]), 2)
        });
        it("should return 3 if parameters [7,2,3,4,5,6] ", function () {
            assert.equal(app.getNotEvenElementsNumber([7,2,3,4,5,6]), 3)
        });
    });
    describe("chnagePartsOfArr", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.chnagePartsOfArr([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.chnagePartsOfArr("some string"), undefined)
        });
        it("should return [3,4,1,8] if parameters [1,8,3,4]" , function () {
            assert.deepEqual(app.chnagePartsOfArr([1,8,3,4]), [3,4,1,8])
        });
        it("should return [3,4,5,2,7,6] if parameters empty string [7,2,6,3,4,5] ", function () {
            assert.deepEqual(app.chnagePartsOfArr([7,2,6,3,4,5]), [3,4,5,7,2,6])

        });
    });
    describe("reverseArr", function () {
        it("if array is empty expected undefined", function() {
            assert.equal(app.reverseArr([]), undefined)
        });
        it("should return undefined if parameters 'some string'", function() {
            assert.equal(app.reverseArr("some string"), undefined)
        });
        it("should return [4,3,8,1] if parameters [1,8,3,4]", function () {
            assert.deepEqual(app.reverseArr([1,8,3,4]), [4,3,8,1])
        });
        it("should return [4,3,2,7] if parameters [7,2,3,4] ", function () {
            assert.deepEqual(app.reverseArr([7,2,3,4]), [4,3,2,7])
        });
    });
    describe("getDayByNumber", function () {
        it("if parameters not Number expected undefined", function() {
            assert.equal(app.getDayByNumber(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getDayByNumber(""), undefined)
        });
        it("should return 'Monday' if parameters 1", function () {
            assert.deepEqual(app.getDayByNumber(1), "Monday")
        });
        it("should return 'Sunday' if parameters 7", function () {
            assert.deepEqual(app.getDayByNumber(7), "Sunday")
        });
        it("should return indefined if parameters -7", function () {
            assert.deepEqual(app.getDayByNumber(-7), undefined)
        });
    });
    describe("getRangeBeetweenTwoPoints", function () {
        it("if parameters not Number expected undefined", function() {
            assert.equal(app.getRangeBeetweenTwoPoints(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getRangeBeetweenTwoPoints("","","",""), undefined)
        });
        it("should return 3.162 if parameters:  x(2,3), y(2, -1) ", function () {
            assert.deepEqual(app.getRangeBeetweenTwoPoints(2,3,2,-1), 3.162)
        });
        it("should return 1 if parameters: x(0,0), y(-2, -1)  ", function () {
            assert.deepEqual(app.getRangeBeetweenTwoPoints(0,0,-2,-1), 1)
        });
    });
    describe("convertNumberToString", function () {
        it("if parameters not Number expected undefined", function() {
            assert.equal(app.convertNumberToString(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.convertNumberToString(""), undefined)
        });
        it("should return четыре if parameters: 4", function () {
            assert.deepEqual(app.convertNumberToString(4), "четыре")
        });
        it("should return двадцать семь  if parameters: 27 ", function () {
            assert.deepEqual(app.convertNumberToString(27), "двадцать семь")
        });
        it("should return триста двадцать семь  if parameters: 327 ", function () {
            assert.deepEqual(app.convertNumberToString(327), "триста двадцать семь")
        });
        it("should return двести if parameters: 200 ", function () {
            assert.deepEqual(app.convertNumberToString(200), "двести ")
        });
    });
    describe("getDay999", function () {
        it("if parameters not Number expected undefined", function() {
            assert.equal(app.getDay999(), undefined)
        });
        it("if parameters empty string expected undefined", function() {
            assert.equal(app.getDay999(""), undefined)
        });
        it("should return wednesday if parameters: 2", function () {
            var result = app.getDay999(2);
            assert.deepEqual(result, "Wednesday")
        });
        it("should return sunday if parameters: 999  ", function () {
            var result = app.getDay999(999);
            assert.deepEqual(result, "Saturday")
        });
    });
    describe("convertorToNumber", function () {
        it("if parameters not Number expected undefined", function() {
            assert.equal(app.convertorToNumber(), undefined)
        });
        it("should return четыре if parameters: 4", function () {
            assert.deepEqual(app.convertorToNumber("четыре"), 4)
        });
        it("should return двадцать семь  if parameters: 27 ", function () {
            assert.deepEqual(app.convertorToNumber("двадцать семь"), 27)
        });
        it("should return триста двадцать семь  if parameters: 327 ", function () {
            assert.deepEqual(app.convertorToNumber("триста двадцать семь"), 327)
        });
        it("should return двести if parameters: 100 ", function () {
            assert.deepEqual(app.convertorToNumber("сто"),  100)
        });
    });
});