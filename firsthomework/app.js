module.exports = {
    getResultEvenNumber: function (a, b) {
        if (typeof a !== "number" && typeof b !== "number") {
            return;
        }
        if (a % 2 === 0) {
            return a * b;
        }
        return a + b;
    },


    getPartByCoordinates: function (x, y) {
        if (typeof x !== "number" && typeof y !== "number") {
            return;
        }
        var message = '';
        if (x === 0 && y === 0) {
            message = "x:0, y:0";
        } else if (x === 0) {
            if (y > 0) {
                message = "x:0, first && second quarter";
            } else {
                message = "x:0, third && forth quarter";
            }
        } else if (y === 0) {
            if (x > 0) {
                message = "y:0, first && second quarter";
            } else {
                message = "y:0, third && forth quarter";
            }
        }

        if (x > 0 && y > 0) {
            message = "first quarter";
        } else if (x < 0 && y > 0) {
            message = "second quarter";
        } else if (x < 0 && y < 0) {
            message = "third quarter";
        } else if (x > 0 && y < 0) {
            message = "forth quarter";
        }

        return message;

    },

    getSumOfPositiveNumbers: function(firstNumber, secondNumber, thirdNumber) {
        if (typeof firstNumber !== "number" && typeof secondNumber !== "number" && typeof thirdNumber !== "number") {
            return;
        }
        var result = 0;

        if (firstNumber > 0) {
            result += firstNumber;
        }
        if (secondNumber > 0) {
            result += secondNumber;
        }
        if (thirdNumber > 0) {
            result += thirdNumber;
        }

        return result;
    },


    maxOfThree: function(a,b,c) {
        if (typeof a !== "number" && typeof b !== "number" && typeof c !== "number") {
            return;
        }
        var sum = a + b + c;
        var multiply = a * b * c;

        if (sum === multiply) {
            return "same value";
        }

        return sum > multiply ? sum + 3 : multiply + 3;
    },

    studyRate: function (rate) {
        if (typeof rate !== "number" || rate < 0) {
            return;
        }
        var message;

        if (rate >= 0 && rate <= 19) {
            message = "F";
        } else if (rate <= 39) {
            message = "E";
        } else if (rate <= 59) {
            message = "D";
        } else if (rate <= 74) {
            message = "B";
        } else if (rate <= 100) {
            message = "A";
        }
        return message;
    },


    sumEvenNumbers: function () {
        var sum = 0;
        for (var i = 1; i <=99; i++) {
            if (i % 2 === 0) {
                sum += i;
            }
        }
        return sum;
    },


    isSimpleNum: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
        var res = true;
        for (var i = 2;i < num; i++) {
            if (num <= 1 || num % i === 0) {
                res = false;
            }
        }
        let number = (num > 1 && res)? "Prime number" : "Not prime number";
        return number;
    },

    getMathSqrt: function (x) {
        if (typeof x !== "number" || x < 0) {
            return;
        }
        var num = 1;
        var res = 0;

        while (x > 0) {
            x -= num;
            num += 2;
            res += (x < 0)? 0 : 1;
        }
        return res;
    },


    getFactorial: function(num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
        var res = 1;
        if (num > 0) {
            for (var i = 1; i <= num; i++) {
                res *= i;
            }
        } else {
            res = 0;
        }
        return res;
    },


    getSumOfNumbers: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
        var sum = 0;

        while (num) {
            var tmp = num % 10;
            num = (num - tmp) / 10;
            sum += tmp;
        }
        return sum;

    },

    reverseNumber: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
        var res = 0;
        while (num > 0) {
            res = res * 10 + num % 10;
            num = ~~(num / 10);
        }
        return res;
    },

    getMaxElementOfArray: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var max = arr[0];
        for (var i = 0; i < arr.length; i++) {

            if (max < arr[i]) {
                max = arr[i];
            }

        }
        return max;
    },

    getMinElementOfArray: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var min = arr[0];
        for (var i = 0; i < arr.length; i++) {

            if (min > arr[i]) {
                min = arr[i];

            }
        }

        return min;
    },

    getMinNumberIndex: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var minIndex = 0 ;
        for (var i = 1; i < arr.length; i++) {

            if (arr[minIndex] > arr[i]) {
                minIndex = i;
            }
        }
        return minIndex;

    },


    getMaxNumberIndex: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var maxIndex = 0 ;
        for (var i = 0; i < arr.length; i++) {

            if (arr[maxIndex] < arr[i]) {
                maxIndex = i;
            }
        }

        return maxIndex;
    },


    bubbleSort: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
    for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[i] > arr[j]) {
                var temp = arr[j];
                var temp2 = arr[i];
                arr[i] = temp;
                arr[j] = temp2;
            }

        }
    }
        return arr;
    },
    insertSort: function (arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
    for (var i = 1; i < arr.length; i++) {
        var min = arr[i];
        var j = i - 1;
        while (j >= 0 && arr[j] > min ) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = min;

    }
        return arr;
    },

    selectSort: function(arr) {
        if (!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        for (var i = 0; i < arr.length; i++) {
            var minIndex = i;
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[i]) {
                    var min = j;
                    var temp = arr[min];
                    arr[min] = arr[i];
                    arr[i] = temp;
                }
            }
        }


        return arr;

    },


    sumOfOddIndexElements: function (arr) {
        if(!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var res = 0;
        for(var i = 1; i < arr.length; i = i + 2){
                res += arr[i];
        }
        return res
    },

    getNotEvenElementsNumber: function (arr) {
        if(!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var counter = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] % 2 !== 0) {
                counter++;
            }
        }
        return counter;
    },

    chnagePartsOfArr: function (arr) {
        if(!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var rest =[];
        for (var i = 0 ; i < arr.length/2; i++) {
            rest[i] = arr[i]
        }
        var rest2 = [];
        var counter = 0;
        for (var i = arr.length/2 ; i < arr.length ; i++) {
            rest2[counter] = arr[i];
            counter++
        }
        return rest2.concat(rest)
    },


    reverseArr: function (arr) {
        if(!Array.isArray(arr) || arr.length === 0) {
            return;
        }
        var res = [];
        var counter = 0;
        for (var i = arr.length - 1; i >= 0; i--) {
            res[counter] = arr[i];
            counter++;
        }
        return res;
    },

    getDayByNumber: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
    var day = ["Monday", "Tuesday", "Wednesday","Thursday","Friday", "Saturday","Sunday"];
    return day[num - 1];
},

    getRangeBeetweenTwoPoints: function(x1,x2,y1,y2) {
        if (typeof x1 !== "number" || typeof x2 !== "number" ||
            typeof y1 !== "number"|| typeof y2 !== "number") {
            return;
        }
    var res = Math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2)).toFixed(3);
        return +res;
},

    convertNumberToString: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
    var result = "";
    arr1 = ['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять'];

        arr2 = ['одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать',
            'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];

        arr3 = [ 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят',
            'семьдесят', 'восемьдесят', 'девяносто'];
    arr4 = ["сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот",
        "семьсот", "восемьсот", "девятьсот"];

    if(num <= 10) {
        result = arr1[num -1];
    } else if(num > 10 && num < 20) {
        result = arr2[num -11];
    } else if(num >= 20 && num < 100) {
        result = arr3[Math.floor(num / 10) -2] + " " + arr1[num % 10 - 1] ;
    } else if(num >= 100 && num < 1000) {
        result = arr4[Math.floor(num / 100) -1] + " " ;
        if (num % 100 === 0 ) {
            result += "";
        } else if (num % 100 <= 10) {
            result += arr1[num % 100 -1];
        } else if (num % 100 > 10 && num % 100 < 20) {
            result += arr2[num % 100 -11];
        } else if (num % 100 >= 20 && num % 100 < 100) {
            result += arr3[Math.floor((num % 100) / 10) -2] + " " + arr1[num % 10 - 1];
        }
    }

    return result;
},

    getDay999: function (num) {
        if (typeof num !== "number" || num < 0) {
            return;
        }
    var day = ["Monday", "Tuesday", "Wednesday","Thursday","Friday", "Saturday","Sunday"]
    var res = [];

    for (var i = 0; i <  num; i++) {

        for (var j = 0; j < day.length; j++) {
            res +=day[j] + " "
        }
    }

    return res.split(" ")[num];
},

    convertorToNumber: function (string) {
    if (string === undefined) {
        return;
    }
    var startString = string.split(" ");
    var res = 0;
    arr1 = ['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять'];
        arr2 = ['одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать',
            'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];

        arr3 = [ 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят',
            'семьдесят', 'восемьдесят', 'девяносто'];
    arr4 = ["сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот",
        "семьсот", "восемьсот", "девятьсот"];
    for (var i = 0; i < arr1.length; i++) {
        if (startString.length === 1) {
            if (startString[0] === arr1[i]) {
                res += (i + 1);
            } else if (startString[0] === arr2[i]) {
                res += (i + 1) + 10;
            } else if (startString[0] === arr3[i]) {
                res += (i + 2) * 10;
            } else if (startString[0] === arr4[i]) {
                res += (i + 1) * 100;
            }
        } else if (startString.length === 2) {
            if (startString[0] === arr3[i]) {
                res += (i + 2) * 10;
            } else if (startString[0] === arr2[i]) {
                res += (i + 2);
            } else if (startString[1] === arr1[i]) {
                res += (i + 1);
            }
        } else {
            if (startString[0] === arr4[i]) {
                res += (i + 1) * 100;
            } else if (startString[1] === arr3[i]) {
                res += (i + 2) * 10;
            } else if (startString[1] === arr2[i]) {
                res += (i + 2);
            } else if (startString[2] === arr1[i]) {
                res += (i + 1);
            }
        }

    }
    return res;
}




}